var through = require("through2");
var pumpify = require("pumpify");

module.exports = function(params)
{
    // Parse and figure out all the parameters.
    if (!params) params = {};
    params.prefix = params.prefix || "";

    function getRelativePath(file)
    {
        return params.prefix + file.data.relativePath.replace("/index.markdown", ".markdown").replace(".markdown", "/");
    }

    // Set up the scanner as an inner pipe that goes through the files and
    // grabs the relative names.
    var relativePaths = [];

    var scanPipe = through.obj(
        function(file, encoding, callback)
        {
            if (file.data)
            {
                var relativePath = getRelativePath(file);
                relativePaths.push(relativePath);
            }

            return callback(null, file);
        },
        function (callback)
        {
            relativePaths.sort();
            callback();
        });

    // We have a second pipe that inserts the relative links, if we found one.
    var updatePipe = through.obj(
        {
            // We need a highWaterMark larger than the totalf iles being processed
            // to ensure everything is read into memory first before writing it out.
            // There is no way to disable the buffer entirely, so we just give it
            // the highest integer value.
            highWaterMark: 2147483647
        },
        function(file, encoding, callback)
        {
            // Figure out the index for this file. If we have it, then get the
            // path before and after it.
            if (file.data)
            {
                // Get the paths, if we have them.
                var relativePath = getRelativePath(file);
                var relativeIndex = relativePaths.indexOf(relativePath);
                var beforePath = relativeIndex > 0 ? relativePaths[relativeIndex - 1] : null;
                var afterPath = relativeIndex >= 0 ? relativePaths[relativeIndex + 1] : null;

                // Set up the related path.
                var relative = {};

                if (beforePath)
                {
                    relative.beforePath = beforePath;
                }

                if (afterPath)
                {
                    relative.afterPath = afterPath;
                }

                // Put the values back so we'll write them out.
                file.page.relative = relative;
            }

            // We are done processing the file.
            return callback(null, file);
        });

    // We have to cork() updatePipe. What this does is prevent updatePipe
    // from getting any data until it is uncork()ed, which we won't do, or
    // the scanPipe gets to the end.
    updatePipe.cork();

    // We have to combine all of these pipes into a single one because
    // gulp needs a single pipe  but we have to treat these all as a unit.
    return pumpify.obj(scanPipe, updatePipe);
}
